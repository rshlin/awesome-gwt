# Spring Boot GWT

Необходимо написать компонент TextField (Текстовое поле) для разных браузеров (как минимум поддержка Chrome и FireFox)

При вводе текста, необходимо что бы каждая следующая буква переворачивалась на 180 градусов (вверх ногами, не четные отображаются нормально, а четные перевернутыми), если текст вводится в середине, то все последующие буквы соответственно, должны перевернуться.

Данное поле должно поддерживать стандартные операции текстового поля (вставка, выделение, вырезание текста и т.д.).


Реализация должна быть в выполнена как компонент GWT.

