package awesome.gwt.client;


import java.util.Objects;

public class TextFlipper {
    public static String flipEven(String s) {
        if (Objects.isNull(s) || s.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];
            boolean isEven = (i + 1) % 2 == 0;
            if (isEven) {
                sb.append(flipChar(c));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    private static char flipChar(char c) {
        switch (c) {
            case 'a':
                return '\u0250';
            case 'b':
                return 'q';
            case 'c':
                return '\u0254';
            case 'd':
                return 'p';
            case 'e':
                return '\u01DD';
            case 'f':
                return '\u025F';
            case 'g':
                return '\u0183';
            case 'h':
                return '\u0265';
            case 'i':
                return '\u0131';
            case 'j':
                return '\u027E';
            case 'k':
                return '\u029E';
            case 'l':
                return '\u0283';
            case 'm':
                return 'ɯ';
            case 'n':
                return 'u';
            case 'p':
                return 'd';
            case 'q':
                return 'ᕹ';
            case 'r':
                return '\u0279';
            case 't':
                return '\u0287';
            case 'u':
                return 'n';
            case 'v':
                return '\u028C';
            case 'w':
                return '\u028D';
            case 'y':
                return '\u028E';
            case '.':
                return '\u02D9';
            case '[':
                return ']';
            case '(':
                return ')';
            case ']':
                return '[';
            case ')':
                return '(';
            case '{':
                return '}';
            case '}':
                return '{';
            case '?':
                return '\u00BF';
            case '!':
                return '\u00A1';
            case '\\':
                return ';';
            case ';':
                return '\\';
            case '<':
                return '>';
            case '_':
                return '\u203E';
            case '\u203F':
                return '\u2040';
            case '\u2045':
                return '\u2046';
            case '\u2234':
                return '\u2235';
            case '\r':
                return '\n';
            case 'а':
                return 'ɐ';
            case 'б':
                return 'ƍ';
            case 'в':
                return 'ʚ';
            case 'г':
                return 'ɹ';
            case 'д':
                return 'ɓ';
            case 'е':
                return 'ǝ';
            case 'ё':
                return 'ǝ';
            case 'ж':
                return 'ж';
            case 'з':
                return 'ε';
            case 'и':
                return 'и';
            case 'й':
                return 'ņ';
            case 'к':
                return 'ʞ';
            case 'л':
                return 'v';
            case 'м':
                return 'w';
            case 'н':
                return 'н';
            case 'о':
                return 'о';
            case 'п':
                return 'u';
            case 'р':
                return 'd';
            case 'с':
                return 'ɔ';
            case 'т':
                return 'ɯ';
            case 'у':
                return 'ʎ';
            case 'ф':
                return 'ȸ';
            case 'х':
                return 'х';
            case 'ц':
                return 'ǹ';
            case 'ч':
                return 'Һ';
            case 'ш':
                return 'm';
            case 'щ':
                return 'm';
            case 'ъ':
                return 'q';
            case 'ы':
                return '?';
            case 'ь':
                return 'q';
            case 'э':
                return 'є';
            case 'ю':
                return '?';
            case 'я':
                return 'ʁ';
            case '1':
                return '\u0196';
            case '2':
                return '\u1105';
            case '3':
                return '\u0190';
            case '4':
                return '\u3123';
            case '5':
                return '\u03DB';
            case '6':
                return '9';
            case '7':
                return '\u3125';
            case '8':
                return '8';
            case '9':
                return '6';
            case '0':
                return '0';
            default:
                return c;
        }
    }
}
