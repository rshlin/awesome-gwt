package awesome.gwt.client;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.canvas.dom.client.CssColor;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

public class AwesomeGwt implements EntryPoint {
    Canvas canvas;
    Context2d context;
    static final int canvasHeight = 300;
    static final int canvasWidth = 300;
    static final String divTagId = "canvasExample"; // must match div tag in html file
    private static final CssColor black = CssColor.make("black");

    public void onModuleLoad() {

        canvas = Canvas.createIfSupported();

        if (canvas == null) {
            RootPanel.get().add(new Label("Sorry, your browser doesn't support the HTML5 Canvas element"));
            return;
        }

        canvas.setStyleName("canvas");     // *** must match the div tag in CanvasExample.html ***
        canvas.setWidth(canvasWidth + "px");
        canvas.setCoordinateSpaceWidth(canvasWidth);

        canvas.setHeight(canvasHeight + "px");
        canvas.setCoordinateSpaceHeight(canvasHeight);

        RootPanel.get().add(canvas);
        context = canvas.getContext2d();

        drawSomethingNew();
    }

    public void drawSomethingNew() {
        context.setFillStyle(black);
        context.setFont("30px Verdana");
        context.fillText("Xxx", 0, 30);
        context.translate(300, 125);
        context.rotate(degree(180));
        context.save();
        context.fillText("adgGFEWagr", 30, 30);
        context.restore();
        context.fill();
    }

    private double degree(int degree) {
        return degree * Math.PI / 180;
    }
}