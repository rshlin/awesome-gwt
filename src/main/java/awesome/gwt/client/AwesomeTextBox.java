package awesome.gwt.client;

import com.google.gwt.user.client.ui.TextBox;

public class AwesomeTextBox extends TextBox {

    private String prevValue;
    public AwesomeTextBox() {
        setDirectionEstimator(false);
        addKeyUpHandler(event -> {
            if (getValue().equals(prevValue)) {
                return;
            }
            int cursorPos = getCursorPos();
            String text = getValue().trim();
            String flipped = TextFlipper.flipEven(text);
            setValue(flipped, false);
            prevValue = flipped;
            setCursorPos(cursorPos);
        });
    }
}
